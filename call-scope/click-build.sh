#!/bin/sh

export LC_ALL=C

CLICKARCH=armhf
rm -rf $CLICKARCH-build
mkdir $CLICKARCH-build
cd $CLICKARCH-build
cmake ..
make DESTDIR=../package install
cd ..
click build package
