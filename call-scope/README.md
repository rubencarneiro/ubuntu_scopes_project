# call-scope
Ubuntu Scope for Calls

# How To Build
cd calls-scope docker run -it -v ${PWD}:/calls-scope clickable/ubuntu-sdk:15.04-armhf bash

# Once Inside the container

apt install libhistoryservice-dev:armhf

# Just run

./click-build.sh
