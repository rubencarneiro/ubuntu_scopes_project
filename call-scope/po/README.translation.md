Add path from root dir to json files with internationalized objects to JSONS, one per line.
  * each internationalized json object key must start with underscore:
    "_dept0": "Sports"

Add path from root dir to each other file with internationalized mesages (source or ini) to POTFILES.in

Create/udpate pot file:
  * run cmake (required intltool package installed)
  * ./make-pot.sh

Update po files:
  * run cmake (required intltool package installed)
  * ./update-pot-from-pos.sh

