/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * Authored by: Kyle Nitzsche <kyle.nitzsche@canonical.com>
 *
 */

#include "query.h"
#include "utils.h"
#include "i18n.h"

#include <unity/scopes/CategorisedResult.h>
#include <unity/scopes/CategoryRenderer.h>
#include <unity/scopes/QueryBase.h>
#include <unity/scopes/SearchReply.h>
#include <unity/scopes/CannedQuery.h>
#include <unity/scopes/VariantBuilder.h>
#include <unity/scopes/Variant.h>
#include <unity/scopes/FilterOption.h>
#include <unity/scopes/OptionSelectorFilter.h>

#include <QDebug>
#include <QStringList>
#include <QLocale>
#include <Qt>
#include <QCoreApplication>

#include <locale.h>

namespace us = unity::scopes;
using namespace History;

static const std::string RENDERER = R"(
{
  "schema-version": 1,
  "template": {
    "category-layout": "grid",
    "card-layout": "horizontal",
    "card-size": "large"
  },
  "components": {
    "title": "title",
    "attributes": {"field": "attributes", "max-count": 4},
    "art": "art_",
    "mascot":  "mascot"
  }
}
)";

static const std::string RENDERER_NO_RESULTS_NON_INTERACTIVE = R"( 
{
  "schema-version": 1,
  "template": {
    "category-layout": "vertical-journal",
    "card-layout": "horizontal",
    "card-size": "small",
    "non-interactive": true
  },
  "components": {
    "title": "title",
    "mascot": "mascot"
  }
}
)";
static const std::string RENDERER_NO_RESULTS = R"( 
{
  "schema-version": 1,
  "template": {
    "category-layout": "vertical-journal",
    "card-layout": "horizontal",
    "card-size": "small"
  },
  "components": {
    "title": "title",
    "mascot": "mascot"
  }
}
)";

QString MISSEDICON;
QString RECEIVEDICON;
QString MADEICON;
QString UNKNOWNICON;

Query::Query(const us::CannedQuery &query, const us::SearchMetadata & metadata, Scope &scope ) :
    SearchQueryBase(query, metadata),
    cannedQuery(query),
    scope_(scope),
    query_(query.query_string())
{

    MISSEDICON = QString("%1/images/missed.svg").arg(QString::fromStdString(scope_.scope_directory()));
    MADEICON = QString("%1/images/placed.svg").arg(QString::fromStdString(scope_.scope_directory()));
    RECEIVEDICON = QString("%1/images/received.svg").arg(QString::fromStdString(scope_.scope_directory()));
    UNKNOWNICON = QString("%1/images/unknownContact.svg").arg(QString::fromStdString(scope_.scope_directory()));

    callType_m[CallType_t::placed] = "placed"; 
    callType_m[CallType_t::received] = "received"; 
    callType_m[CallType_t::missed] = "missed"; 
}

Query::~Query()
{
}

void Query::cancelled()
{
}

QString strong(QString str) {
    return QString("<strong>%1</strong>").arg(str);
}

Events Query::getEventsForDateRange(QDate start, QDate end, EventViewPtr eView, Events &ev_l)
{
    // in order to optimize the parsing, this functions reuses the events that
    // were previously retrieved and then if necessary, get extra events
    qDebug() << "=== CALLS STARTING getEventsforDateRange";
    qDebug() << "=== CALLS range start date: " << start;
    qDebug() << "=== CALLS range end end: " << end;
    Events l_toadd;
    try {
        // start by parsing the existing events
        Q_FOREACH(const Event &event, ev_l) {
            QDate ev_date = event.timestamp().date();

            // if the date is newer than the end date, keep searching
            // as the results are sorted from newer to older
            if (ev_date > end) {
                continue;
            }

            // now if the date is older than the end date, we are already
            // out of bounds and can stop processing
            if (ev_date < start) {
                qDebug() << "=== CALLS extant events. out of bounds";
                break;
            }

            // if we got here, we want the event
            qDebug() << "=== CALLS. extant events. adding event timestamped: " << ev_date.toString();
            l_toadd.append(event);
        }

        bool going = true;
        while (going){
            Events l = eView->nextPage();

	    if (l.size() == 0)
                going = false;
                // append the events to the existing ones for the next query
            ev_l.append(l);

                // and process them too
            Q_FOREACH(const Event &event, l) {
                QDate ev_date = event.timestamp().date();

                // if the date is newer than the end date, keep searching
                // as the results are sorted from newer to older
                if (ev_date > end) {
                    continue;
                }

                // now if the date is older than the end date, we are already
                // out of bounds and can stop processing
                if (ev_date < start) {
                    qDebug() << "=== CALLS new event. out of bounds";
                    going = false;
                    break;
                }

                // if we got here, we want the event
                l_toadd.append(event);
                qDebug() << "=== CALLS. new events. adding event timestamped: " << ev_date.toString();
            }
        }
    }
    catch (std::exception &e){
        qDebug() << "=== calls exception caught";
    }
    qDebug() << "=== CALLS: num events: " << QString::number(l_toadd.size());
    qDebug() << "=== CALLS ENDING getEventsforDateRange";
    return l_toadd;
}

void Query::processEvents(Events &ev_l, std::set<std::string> &call_types)
{
    qDebug()<<"==== CALLS. process. num events: " << QString::number(ev_l.size());
    //loop through voice events and build data structures
    for (VoiceEvent ev : ev_l) {
        vevent_t vevent;
        QDate cDate = ev.timestamp().date();
        QString date_s = cDate.toString("dd-MM-yyyy");
        QTime time = ev.timestamp().time();
        QString time_s = time.toString("hh:mm");


        //decide which category to use based on the call date
        //Note: for each surfaced category (today, previous 7, previous 30 and
        //older), there are a set of vars. Depending on the date of the voice
        //event, we assign a set pointers to the proper vars, then we use the
        //pointers. This allows simply using the pointers while knowning they
        //correspond to the correct cateogory-specific vars.
        //Also: calls in the same surfaced  category involving the same other
        //phone number and of the same type (missed, placed, or received) are
        //"collapsed": that is, in results they appear together once with a
        //number indicating how many there are and in the preview the details of
        //all such calls are shown.
        if (cDate == today){
            date_s = _("Today");
            cat = cat1;//the categoryRenderer for today
            vevents = &vevents1;//the list of *surfaced* calls for today, not including dups ("collapsed" calls)
            ids = &ids1;//the set of ids for surfaced calls.
            id_collEvs = &id_collEvs1;//maps the surfaced call id to a list of its collapsed calls
        } else if (cDate >= sevenDaysStart) {
            cat = cat7;
            vevents = &vevents7;
            ids = &ids7;
            id_collEvs = &id_collEvs7;
        } else if (cDate >= thirtyDaysStart) {
            cat = cat30;
            vevents = &vevents30;
            ids = &ids30;
            id_collEvs = &id_collEvs30;
        }

        vevent.cat = cat;

        QTime duration = ev.duration();
        QString duration_ = QString("%1: %2").arg(strong(_("Duration"))).arg(duration.toString());

        //get call participants and deduce the caller
        Participant caller = ev.participants().first();
        QString callerId = caller.identifier();
        QString alias = caller.alias().isEmpty() ? callerId : caller.alias();

        QString query = qstr(query_);
        if (caller.contactId().isEmpty()) { //not a contact
            if (callerId == "x-ofono-unknown") {
                //Translators: unknown caller, meaning there is no information about the person who placed the call
                alias = qstr(_("Unknown"));
            }
            else if (callerId == "x-ofono-private") {
                //Translators: private caller, meaning the person who placed the call has kept their phone number info private
                alias = qstr(_("Private"));
            }
            vevent.art = UNKNOWNICON;
            vevent.phone = alias;
            if (searchingName) { //if searching on name, continue if not found, allow searcing on _("Unknown")
                if (!alias.contains(query,Qt::CaseInsensitive))
                    continue;
            }
        } else { //is a contact
            if (searchingName) {
                bool match = true;
                Q_FOREACH(const QString &part, parts) {
                    if (!alias.contains(part, Qt::CaseInsensitive)) {
                        match = false;
                        break;
                    }
                }
                if (!match) {
                    continue;
                }
            }
            QString avatar = caller.avatar();
            if (avatar.isEmpty()) {
                avatar = UNKNOWNICON;
            }
            vevent.art = avatar;
        }
        // continue if searching on date and it is not matched
        if ((searchingDate) && (cDate != searchDate))
            continue;
        vevent.caller = alias;
        vevent.time = QString("%1").arg(time_s);
        vevent.timeShort = time_s;
        vevent.date = QString("%1").arg(date_s);
        vevent.dateShort = date_s;

        std::string missed = "";
        if (ev.missed()) {
            vevent.type = CallType_t::missed;
            vevent.mascot = MISSEDICON;
            vevent.typeMsg = strong(_("Missed"));
            vevent.duration = "";
        } else if (ev.senderId() == "self") {
            vevent.type = CallType_t::placed;
            vevent.mascot = MADEICON;
            vevent.typeMsg = strong(_("Placed"));
            vevent.duration = duration_;
        } else {
            vevent.type = CallType_t::received;
            vevent.mascot = RECEIVEDICON;
            vevent.typeMsg = strong(_("Received"));
            vevent.duration = duration_;
        }

        // do not show call if it does not match user call type filter, if any
        if (!call_types.empty()) {
              if (call_types.find(callType_m[vevent.type]) == call_types.end())  
			continue;
        }

        vevent.msgUri = QString("message:///%1").arg(callerId);
        vevent.callUri = QString("tel:///%1").arg(callerId);

        //calls with the same person of the same type (missed, placed, received)
        //are collapsed into one entry per category (today, previous 7, previous
        //30 in surfaced results. Here we make an id from the caller
        //and the type to use as a key to see whether the event exists in the
        //current category or not. If it exists, append the event to the
        //list associated with the id in current categories id_collEvs map.
        QString id = QString("%2-%3").arg(callerId).arg(vevent.typeMsg);
        vevent_t * ce;
        vevent.collapseId = id;
        if (ids->contains(id)) {//a collapse event
            collapsedEvent_t cev;
            cev.time = vevent.time;
            cev.date = vevent.date;
            cev.duration = vevent.duration;
            (*id_collEvs)[id].append(cev);
        } else {//a new event
            ids->append(id);
            vevents->append(vevent);
        }
    }
}

void Query::run(us::SearchReplyProxy const& reply)
{

    qWarning() << "CALLS starting";
    textdomain(GETTEXT_DOMAIN.toStdString().c_str());

    auto filter_state = query().filter_state();

    //Translators: This is the title of the call type filter. This filter lets the user display only the specified type of call: received, placed, or missed
    us::OptionSelectorFilter::SPtr callType_filter = us::OptionSelectorFilter::create("callType", _("Call type"), true);
    callType_filter->add_option("received", _("Received"));
    callType_filter->add_option("placed", _("Placed"));
    callType_filter->add_option("missed", _("Missed"));

    reply->push({callType_filter}, query().filter_state());

    if (callType_filter->has_active_option(filter_state)) {
        auto textTypes = callType_filter->active_options(filter_state);
        for (auto const &opt: textTypes) {
            call_types.insert(opt->id());
        }
    }

    const QString scopePath = QString::fromStdString(scope_.scope_directory());
    QString tdir = QString("%1/locale/").arg(scopePath);
    QString basedir = QString(bindtextdomain(GETTEXT_DOMAIN.toStdString().c_str(), tdir.toStdString().c_str()));

    setlocale(LC_ALL, "");

    // History uses d-bus so we have to set this up even though we don't
    // use it, per pattern in unity-scope-contacts
    int argc = 1;
    char* argv = "calls-scope";
    QCoreApplication *app = new QCoreApplication(argc, &argv);

    us::CategoryRenderer renderer(RENDERER);
    us::CategoryRenderer renderer_no_results(RENDERER_NO_RESULTS);
    us::CategoryRenderer renderer_no_results_non_interactive(RENDERER_NO_RESULTS_NON_INTERACTIVE);

    //create categories for each surfacing time frame
    cat1_no_results = reply->register_category("today_no_results:interactive", _("Today"), "", renderer_no_results);
    cat1_no_results_non_interactive = reply->register_category("today_no_results:noninteractive", _("Today"), "", renderer_no_results_non_interactive);
    cat1 = reply->register_category("today", _("Today"), "", renderer);
    cat7 = reply->register_category("previous7", _("Previous 7 days"), "", renderer);
    cat30 = reply->register_category("previous30", _("Previous 30 days"), "", renderer);
    //create pointer to category. This will point at the right/current category based
    //on the date of each call
    us::Category::SCPtr cat;

    searchingDate = false;
    searchingName = false;
    // handle search:
    if (query_ != ""){
        QString qry = qstr(query_);
        //let query use spaces or dashes to delimit token
        QStringList partsSpaces = qry.split(" ");
        QStringList partsDashes = qry.split("-");
        if (partsSpaces.size() >= partsDashes.size())
            parts = partsSpaces;
        else
            parts = partsDashes;
        //when user enters delimeter char (and before they enter more)
        //delete the final empty string
        if ((parts.size() > 1) && (parts[parts.size()-1] == ""))
            parts.removeLast();
        int dy = 0;
        int mo = 0;
        int yr = 0;
        if (parts.size() > 0) {
            bool searchStartsWithInt;
            bool *anInt = & searchStartsWithInt;
            dy = parts[0].toInt(anInt);
            if (searchStartsWithInt) {
                //treat search as date
                searchingDate = true;
                if (parts.size() > 1)
                    mo = parts[1].toInt();
                if (parts.size() > 2)
                    yr = parts[2].toInt();
                QDate date = QDate::currentDate();
                if (mo == 0 )
                    mo = date.month();
                if (yr < 2000)
                    yr = date.year();
                searchDate = QDate(yr, mo, dy);
            } else {
                //treat search as name
                searchingName = true;
            }
        }
    }

    today = QDate::currentDate();
    sevenDaysEnd = today.addDays(-1);
    sevenDaysStart = today.addDays(-7);
    thirtyDaysEnd = sevenDaysStart.addDays(-1);
    thirtyDaysStart = today.addDays(-37);

    const Sort sort = Sort("timestamp", Qt::SortOrder::DescendingOrder,Qt::CaseInsensitive);
    Manager *manager = Manager::instance();
    EventViewPtr eView = manager->queryEvents(EventType::EventTypeVoice, sort);

    Events ev_l;
    Events filtered = getEventsForDateRange(today, today, eView, ev_l);
    processEvents(filtered, call_types);

    bool agged = false;
    if (search_metadata().is_aggregated())
    {
        auto keywords = search_metadata().aggregated_keywords();
        if (keywords.find("recent") != keywords.end())
        {
            agged = true;
            qDebug() << "==== CALLS is agg'd by keyword: recent";
            if (vevents1.size() == 0)
            {
                qDebug() << "=== CALLS NONE today";
                us::CategorisedResult res(cat1_no_results);
                res["title"] = _("No Calls Today");
                res["mascot"] = RECEIVEDICON.toStdString();
                res.set_uri("scope://com.canonical.scopes.calls_calls");
                reply->push(res);
                return;
            }
            for (auto ev : vevents1)
            {
                us::CategoryRenderer renderer(RENDERER);
                cat = reply->register_category("aggregated_keyword+one_message", "Today", "", renderer_no_results);
                unity::scopes::CategorisedResult res(cat);
                res.set_title(ev.caller.toStdString());
                res.set_uri("scope://com.rubencarneiro.scopes.calls_calls");
                if (ev.type == CallType_t::received)
                    res["mascot"] = RECEIVEDICON.toStdString();
                else if (ev.type == CallType_t::missed) 
                    res["mascot"] = MISSEDICON.toStdString();
                else 
                    res["mascot"] = MADEICON.toStdString();
                us::VariantBuilder attrs;
                attrs.add_tuple({
                    {"value", us::Variant(QString("%1").arg(ev.timeShort).toStdString())}
                });
                attrs.add_tuple({
                    {"value", us::Variant("")}
                });
                res["attributes"] = attrs.end();

                reply->push(res);
                return;
            }
        }
    }
    if (!agged)
    {
        // if no messages today, send generated result saying No Messages Today
        if (vevents1.size() == 0){
                qDebug() << "=== CALLS NONE today";
                us::CategorisedResult res_(cat1_no_results_non_interactive);
                res_["title"] = _("No Calls Today");
                res_.set_uri("null");
                reply->push(res_);
        }
        else
            Query::pushReplies(vevents1, id_collEvs1, reply);

        filtered = getEventsForDateRange(sevenDaysStart, sevenDaysEnd, eView, ev_l);
        processEvents(filtered, call_types);
        Query::pushReplies(vevents7, id_collEvs7, reply);

        filtered = getEventsForDateRange(thirtyDaysStart, thirtyDaysEnd, eView, ev_l);
        processEvents(filtered, call_types);
        Query::pushReplies(vevents30, id_collEvs30, reply);
    }
}

void Query::pushReplies(QList<vevent_t> &vevents, QMap<QString,QList<collapsedEvent_t>> &id_collEvs, us::SearchReplyProxy const& reply) {

    QListIterator<Query::vevent_t> iter(vevents);
    while (iter.hasNext()){
        vevent_t vev = iter.next();
        us::CategorisedResult res(vev.cat);

        //details holds call details for at least one call, but then also for
        //collapsed calls, if any
        using namespace unity::scopes;

        // to populate the preview table for call details, we need a VariantArray of VariantArrays
        VariantArray details;

        VariantArray values;
        values = {Variant(QString("%1 %2").arg(vev.date).arg(vev.time).toStdString()), Variant(vev.duration.toStdString())};
        details.emplace_back(values);

        //if there are collapsed calls, display the number in the title
        int collapses = id_collEvs[vev.collapseId].size();
        if ( collapses == 0 ) {
            res.set_title(vev.caller.toStdString());
        } else {
            res.set_title(QString("(%1) %2").arg(++collapses).arg(vev.caller).toStdString());
            // get the collapsed call details
            QListIterator<Query::collapsedEvent_t> iter(id_collEvs[vev.collapseId]);
            while (iter.hasNext()) {
                collapsedEvent_t vev = iter.next();
                values = {Variant(QString("%1 %2").arg(vev.date).arg(vev.time).toStdString()), Variant(vev.duration.toStdString())};
                details.emplace_back(values);
            }
        }

        res["details"] = Variant(details);

        us::VariantBuilder attrs;
        attrs.add_tuple({
            {"value", us::Variant(QString("%1 %2").arg(vev.timeShort, vev.dateShort).toStdString())}
        });
        attrs.add_tuple({
            {"value", us::Variant("")}
        });
        res["attributes"] = attrs.end();

        res["art_"] = vev.art.toStdString();
        // 'art_' used (instead of art) to enable disambiguation in aggregator
        // scope. Do not change field name.
        res["mascot"] = vev.mascot.toStdString();
        res["type"] = vev.typeMsg.toStdString();

        us::VariantBuilder actions;
        //Translators: "Call" is a verb, as in to dial a phone number
        std::string call = _("Call");
        //Translators: "Message" is a verb, as in to send a text message
        std::string message = _("Message");
        actions.add_tuple({
                              {"id", us::Variant("call")},
                              {"label", us::Variant(call)},
                              {"uri", us::Variant(vev.callUri.toStdString())}
                          });
        actions.add_tuple({
                              {"id", us::Variant("sms")},
                              {"label", us::Variant(message)},
                              {"uri", us::Variant(vev.msgUri.toStdString())}
                          });
        res["actions"]=actions.end();

        res.set_uri(vev.callUri.toStdString());
        res["phone"] = QString("%1: %2").arg(strong(_("Number"))).arg(vev.phone).toStdString();
        reply->push(res);
    }
}
