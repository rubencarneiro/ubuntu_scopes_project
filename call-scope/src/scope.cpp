/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * Authored by: Kyle Nitzsche <kyle.nitzsche@canonical.com>
 *
 */

#include "scope.h"
#include "query.h"
#include "preview.h"
#include "utils.h"

#include <unity-scopes.h>

#include <QCoreApplication>
#include <QThread>
#include <QDebug>
#include <History/eventview.h>

namespace us = unity::scopes;
//using namespace History;

Scope::Scope()
{
}

void Scope::start(std::string const&)
{
}

void Scope::stop()
{
}

void Scope::run()
{

}

us::SearchQueryBase::UPtr Scope::search(us::CannedQuery const &q,
                                        us::SearchMetadata const &metadata )
{
    us::SearchQueryBase::UPtr cannedQuery(new Query(q,
                                          metadata,
                                          *this
                                          ));
    return cannedQuery;
}

us::PreviewQueryBase::UPtr Scope::preview(us::Result const& result, us::ActionMetadata const& metadata ) {
    us::PreviewQueryBase::UPtr preview(new Preview(result, metadata));
    return preview;
}

#define EXPORT __attribute__ ((visibility ("default")))

extern "C"
{

    EXPORT
    us::ScopeBase*
    // cppcheck-suppress unusedFunction
    UNITY_SCOPE_CREATE_FUNCTION()
    {
        return new Scope();
    }

    EXPORT
    void
    // cppcheck-suppress unusedFunction
    UNITY_SCOPE_DESTROY_FUNCTION(us::ScopeBase* scope_base)
    {
        delete scope_base;
    }

}
