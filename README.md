#UBuntu Scopes Project
========================

**Please note that scopes will be fading from ubuntu touch, till OTA-8 Or OTA-9 API will be removed.**
**So theres not much we can do**

A collection of scopes that i am trying to maintain to ubuntu 16.04

- Like my work buy me a coffe by Donation to my Paypal https://paypal.me/rubencarneiro.

- Join the Telegram group at https://t.me/joinchat/CUStXw4BvzPjZyz1lGOyUQ

- Please note most scopes wont work, im working on it. as soon any scope works, i will release a build.

**How to Build**:

========================

- First will create a xenial armhf VM.
   - sudo apt-get update
   - sudo apt-get install qemu-user-static
   - sudo qemu-debootstrap --arch armhf xenial eabi-chroot

**After to enter on the root**
- sudo chroot eabi-chroot

**You Can Now use it to build your packages fot UT or Scopes**
- You need to edit the root /etc/apt/source.list and add:
  - deb http://ports.ubuntu.com/ubuntu-ports xenial multiverse
  - deb http://ports.ubuntu.com/ubuntu-ports xenial universe

  **We need to install the app or scope dependencies**
