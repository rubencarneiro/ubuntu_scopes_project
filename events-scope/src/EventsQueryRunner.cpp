/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EventsQueryRunner.h"
#include "i18n.h"

#include <unity/scopes/CategoryRenderer.h>
#include <unity/scopes/QueryBase.h>
#include <unity/scopes/SearchReply.h>
#include <unity/scopes/VariantBuilder.h>

#include <QOrganizerEventTime>
#include <QOrganizerItem>
#include <QOrganizerItemDescription>
#include <QOrganizerItemDetailFieldFilter>
#include <QOrganizerItemDisplayLabel>
#include <QOrganizerItemSortOrder>
#include <QOrganizerItemUnionFilter>
#include <QDebug>

using namespace unity::scopes;
using namespace QtOrganizer;

EventsQueryRunner::EventsQueryRunner(QString scopeDir,
        const SearchReplyProxy* reply,
        QDateTime const& start,
        QDateTime const& end,
        QString const& query,
        const char* categoryString,
        const QString& translatedCategoryString)
    : mScopeDir(scopeDir), mCategoryString(categoryString),
      mTranslatedCategoryString(translatedCategoryString),
      mReply(reply),mStartDate(start), mEndDate(end),
      mQuery(query),manager("eds")
{
    QOrganizerItemSortOrder sortAllDay;
    sortAllDay.setDirection(Qt::SortOrder::AscendingOrder);
    sortAllDay.setDetail(QOrganizerItemDetail::TypeEventTime,
                         QOrganizerEventTime::FieldAllDay);
    QOrganizerItemSortOrder sortStartTime;
    sortStartTime.setDirection(Qt::SortOrder::AscendingOrder);
    sortStartTime.setDetail(QOrganizerItemDetail::TypeEventTime,
                            QOrganizerEventTime::FieldStartDateTime);
    sorting << sortAllDay << sortStartTime;

    QOrganizerItemDetailFieldFilter titleFilter;
    titleFilter.setDetail(QOrganizerItemDetail::TypeDisplayLabel, QOrganizerItemDisplayLabel::FieldLabel);
    titleFilter.setMatchFlags(QOrganizerItemFilter::MatchContains);
    titleFilter.setValue(query);

    QOrganizerItemDetailFieldFilter descFilter;
    descFilter.setDetail(QOrganizerItemDetail::TypeDescription, QOrganizerItemDescription::FieldDescription);
    descFilter.setMatchFlags(QOrganizerItemFilter::MatchContains);
    descFilter.setValue(query);

    filter = QOrganizerItemUnionFilter();
    filter.append(titleFilter);
    filter.append(descFilter);

    hint = QOrganizerItemFetchHint();
    QList<QOrganizerItemDetail::DetailType> types;
    types << QOrganizerItemDetail::TypeDisplayLabel << QOrganizerItemDetail::TypeDescription << QOrganizerItemDetail::TypeEventTime;
    hint.setDetailTypesHint(types);
    hint.setOptimizationHints(QOrganizerItemFetchHint::NoActionPreferences | QOrganizerItemFetchHint::NoBinaryBlobs);
}




void EventsQueryRunner::getResults()
{

    QList<QOrganizerItem> items_init;
    QList<QOrganizerItem> items;
    QDate today = QDate::currentDate();
    int itemCount = 0;
    int maxItemCount = 10;
    QString art = QString(mScopeDir).append("/images/calendar.png");
    CategoryRenderer renderer(CATEGORY_EVENTS);

    items_init = manager.items(mStartDate, mEndDate, filter, maxItemCount, sorting, hint);

    //The following is needded because an all day event
    //has an end day that is the start of the next day.
    //So, when hours, minutes and seconds are all 0, it means
    //this event ends at the first instance of the next day.
    //In these cases it should NOT be counted as an event on 
    //that next day, so we adjust the day (addDays(-1)) 
    foreach (const QOrganizerItem &item, items_init) {
        QOrganizerEventTime eventTime = item.detail(QOrganizerItemDetail::TypeEventTime);
        if (!eventTime.isEmpty()) {
            QString eventId = item.guid();
            auto end = eventTime.endDateTime().date();
            auto endTime = eventTime.endDateTime().time();
            if (endTime.hour() == 0 &&
                endTime.minute() == 0 &&
                endTime.second() == 0){
                end = end.addDays(-1);
                if (end < mStartDate.date())
                    continue;
            }
            items.append(item);
        }
    }
            
    QString categoryLabel = QString("%1: %2").arg(mTranslatedCategoryString).arg(items.length());

    if (items.length() == 0) {
            auto categoryToday = (*mReply)->register_category(mCategoryString, categoryLabel.toStdString(), "", renderer);
            CategorisedResult result(categoryToday);    
            result["uri"]      = "scope://com.rubencarneiro.scopes.events_events";
            result["title"]    = _("No events");
            result["subtitle"] = "";
            result["summary"]  = "";
            result["art"]      = art.toStdString();
            (*mReply)->push(result);
            return;
    }
    auto categoryToday = (*mReply)->register_category(mCategoryString, categoryLabel.toStdString(), "", renderer);

    foreach (const QOrganizerItem &item, items) {
        QOrganizerEventTime eventTime = item.detail(QOrganizerItemDetail::TypeEventTime);
        QOrganizerItemDescription description = item.detail(QOrganizerItemDetail::TypeDescription);

        if (!eventTime.isEmpty()) {
            QString eventId = item.guid();

            auto start = eventTime.startDateTime().date();
            auto end = eventTime.endDateTime().date();
            
            QString dayLabel = QDate::longDayName(start.dayOfWeek());
            QString timeLabel = eventTime.startDateTime().toLocalTime().time().toString("hh:mm");

            QString title = item.displayLabel();
            QString summary = description.description();

            itemCount++;

            QString time;
            if (start == today) {
                time = eventTime.isAllDay() ? "" : timeLabel;
            } else {
                time = eventTime.isAllDay() ?
                            dayLabel : QString("%1 - %2").arg(timeLabel).arg(dayLabel);
            }

            CategorisedResult result(categoryToday);
            result["uri"]      = "uri";
            result["title"]    = title.toStdString();
            result["subtitle"] = time.toStdString();
            result["summary"]  = summary.toStdString();
            result["art"]      = art.toStdString();

            // TODO Verify this works:
            // https://code.launchpad.net/~rpadovani/ubuntu-calendar-app/1321695/+merge/220463
            
	    // We lost prefix format(qtorganizer:eds::system-calendar) for a eventId when eds parsing.
	    // So calendar won't open the regarding event even if the MP as above is applied.
	    // https://code.launchpad.net/~gary-wzl77/ubuntu-calendar-app/add_prefix/+merge/267317
            QString openUri = QString("calendar://eventId=%1").arg(eventId);
            VariantBuilder actions;
            actions.add_tuple({
                {"id", Variant("open")},
                {"label", Variant(_("Open"))},
                {"uri", Variant(openUri.toStdString())}
            });
            result["actions"] = actions.end();

            bool sendMore = (*mReply)->push(result);
            if (!sendMore) {
                break;
            }
            if (itemCount >= maxItemCount) break;
        }
    }
}
