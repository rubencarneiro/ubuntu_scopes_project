/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unity-scope-events.h"
#include "unity-scope-events-query.h"
#include "unity-scope-events-preview.h"
#include "i18n.h"

#include <QCoreApplication>

using namespace unity::scopes;

void EventsScope::start(std::string const&)
{
    setlocale(LC_ALL, "");
    textdomain(GETTEXT_DOMAIN.toStdString().c_str());
    scopeDir = QString::fromStdString(scope_directory());
    QString translations = QString("%1/locale/").arg(scopeDir);
    bindtextdomain(GETTEXT_DOMAIN.toStdString().c_str(),
                translations.toStdString().c_str());
}

void EventsScope::run() {
    int zero = 0;
    app = new QCoreApplication(zero, nullptr);
    app->exec();
    delete app;
}

void EventsScope::stop()
{
    app->quit();
}

SearchQueryBase::UPtr EventsScope::search(CannedQuery const &q, SearchMetadata const& metadata)
{
    SearchQueryBase::UPtr query(new EventsQuery(q, metadata, scopeDir));
    return query;
}

PreviewQueryBase::UPtr EventsScope::preview(Result const& result, ActionMetadata const& metadata) {
    PreviewQueryBase::UPtr preview(new EventsPreview(result, metadata));
    return preview;
}

#define EXPORT __attribute__ ((visibility ("default")))

extern "C"
{

    EXPORT
    unity::scopes::ScopeBase*
    // cppcheck-suppress unusedFunction
    UNITY_SCOPE_CREATE_FUNCTION()
    {
        return new EventsScope();
    }

    EXPORT
    void
    // cppcheck-suppress unusedFunction
    UNITY_SCOPE_DESTROY_FUNCTION(ScopeBase* scope_base)
    {
        delete scope_base;
    }

}
