/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DEMOSCOPE_H
#define DEMOSCOPE_H

#include <unity/scopes/ScopeBase.h>
#include <unity/scopes/QueryBase.h>
#include <unity/scopes/PreviewQueryBase.h>
#include <QString>

using namespace unity::scopes;

class QCoreApplication;

class EventsScope : public ScopeBase
{
public:
    virtual void start(std::string const&) override;

    virtual void run() override;

    virtual void stop() override;

    PreviewQueryBase::UPtr preview(
            const Result&, const ActionMetadata&) override;

    virtual SearchQueryBase::UPtr search(
            CannedQuery const&, SearchMetadata const&) override;

private:
    QCoreApplication *app;
    QString scopeDir;
};

#endif
