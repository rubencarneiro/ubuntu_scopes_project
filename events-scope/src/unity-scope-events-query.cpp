/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "unity-scope-events-query.h"
#include "EventsQueryRunner.h"
#include "i18n.h"

#include <unity/scopes/SearchReply.h>
#include <QDebug>

using namespace unity::scopes;

const char* TODAY_CATEGORY_ID = "Events today";
const char* UPCOMING_CATEGORY_ID = "Upcoming Events";

QTORGANIZER_USE_NAMESPACE

Q_DECLARE_METATYPE(QOrganizerItem)

EventsQuery::EventsQuery(CannedQuery const& query, SearchMetadata const& metadata, QString scopeDir)
    : SearchQueryBase(query, metadata), scopeDir(scopeDir)
{
    setlocale(LC_ALL, "");
    textdomain(GETTEXT_DOMAIN.toStdString().c_str());

    qRegisterMetaType<QOrganizerItem>("QOrganizerItem");
}

EventsQuery::~EventsQuery()
{
}

void EventsQuery::cancelled()
{
}

void EventsQuery::run(SearchReplyProxy const& reply)
{
    CannedQuery query = SearchQueryBase::query();

    QString queryString = QString::fromStdString(query.query_string()).toLower();
    QDate today = QDate::currentDate();

    if (!queryString.isEmpty()) {
        QString label = QString(_("Results for: %1")).arg(queryString);

        QDateTime searchStart(today);
        QDateTime searchEnd(searchStart.addYears(1));

        EventsQueryRunner *searchRunner = new EventsQueryRunner(
                // Note, 'Events today' here is just a hardcoded category id.
                // It may not refer to events from this day.
                scopeDir, &reply, searchStart, searchEnd, queryString, TODAY_CATEGORY_ID, label);
        searchRunner->getResults();
    } else {
        QDateTime todayStart(QDateTime::currentDateTime());
        QString label = _(TODAY_CATEGORY_ID);
        QDateTime todayEnd = QDateTime(today, QTime(23, 59, 59));

        EventsQueryRunner *todayRunner = new EventsQueryRunner(
                scopeDir, &reply, todayStart, todayEnd, "", TODAY_CATEGORY_ID, label);
        todayRunner->getResults();  
        QDate tomorrow = today.addDays(1);
        QDateTime upcomingStart(tomorrow);
        QDateTime upcomingEnd(tomorrow.addDays(1), QTime(23, 59, 59));
        QString up_label = _(UPCOMING_CATEGORY_ID);

        EventsQueryRunner *upcomingRunner = new EventsQueryRunner(
                scopeDir, &reply, upcomingStart, upcomingEnd, "", UPCOMING_CATEGORY_ID, up_label);
        upcomingRunner->getResults();
    }
}
