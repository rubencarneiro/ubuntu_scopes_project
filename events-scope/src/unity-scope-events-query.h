/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVENTS_QUERY_H
#define EVENTS_QUERY_H

#include "EventsQueryRunner.h"

#include <unity/scopes/SearchQueryBase.h>
#include <unity/scopes/ReplyProxyFwd.h>


using namespace unity::scopes;

class EventsQuery : public SearchQueryBase
{
public:
    EventsQuery(CannedQuery const&, SearchMetadata const&, QString);
    ~EventsQuery();
    virtual void cancelled() override;
    virtual void run(SearchReplyProxy const& reply) override;
private:
    QString scopeDir;
};

#endif
