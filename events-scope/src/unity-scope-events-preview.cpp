/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include"unity-scope-events-preview.h"

#include<unity/scopes/PreviewWidget.h>
#include<unity/scopes/ColumnLayout.h>
#include<unity/scopes/PreviewReply.h>

using namespace unity::scopes;

EventsPreview::EventsPreview(Result const& result, ActionMetadata const& metadata)
    : PreviewQueryBase(result, metadata)
{
}

EventsPreview::~EventsPreview()
{
}

void EventsPreview::cancelled()
{
}

void EventsPreview::run(PreviewReplyProxy const& reply)
{
    ColumnLayout oneColumnLayout(1);
    oneColumnLayout.add_column({"headerId", "descriptionId", "actionsId"});

    reply->register_layout({oneColumnLayout});

    PreviewWidget header("headerId", "header");
    header.add_attribute_mapping("title", "title");
    header.add_attribute_mapping("subtitle", "subtitle");

    PreviewWidget text("descriptionId", "text");
    text.add_attribute_mapping("text", "summary");

    PreviewWidget actions("actionsId", "actions");
    actions.add_attribute_mapping("actions", "actions");

    reply->push({header, text, actions});
}

