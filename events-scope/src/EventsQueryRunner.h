/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef EVENTSQUERYRUNNER_H
#define EVENTSQUERYRUNNER_H

#include <unity/scopes/Category.h>
#include <unity/scopes/CategorisedResult.h>
#include <unity/scopes/SearchQueryBase.h>
#include <unity/scopes/ReplyProxyFwd.h>

#include <QEvent>
#include <QObject>
#include <QOrganizerItemFetchHint>
#include <QOrganizerItemSortOrder>
#include <QOrganizerItemUnionFilter>
#include <QOrganizerManager>

const std::string CATEGORY_EVENTS = R"(
{
  "schema-version": 1,
  "template": {
    "category-layout": "grid",
    "card-layout": "horizontal",
    "card-size": "medium"
  },
  "components": {
    "title": "title",
    "subtitle": "subtitle",
    "art": "art"
  }
}
)";

using unity::scopes::Category;
using unity::scopes::SearchReplyProxy;
using QtOrganizer::QOrganizerItemFetchHint;
using QtOrganizer::QOrganizerItemUnionFilter;
using QtOrganizer::QOrganizerItemSortOrder;
using QtOrganizer::QOrganizerManager;

class EventsQueryRunner
{
public:

    EventsQueryRunner(
               QString scopeDir,
               const SearchReplyProxy* reply,
               QDateTime const& start,
               QDateTime const& end,
               QString const& query,
               const char* categoryString,
               const QString& translatedCategoryString);

    void getResults();

private:
    QString mScopeDir;
    const char* mCategoryString;
    QString mTranslatedCategoryString;
    const SearchReplyProxy* mReply;

    QDateTime mStartDate;
    QDateTime mEndDate;
    QString mQuery;

    QOrganizerManager manager;
    QList<QOrganizerItemSortOrder> sorting;
    QOrganizerItemUnionFilter filter;
    QOrganizerItemFetchHint hint;
};

#endif // EVENTSQUERYRUNNER_H
