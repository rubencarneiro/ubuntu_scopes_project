#!/bin/bash

rm -rf click
mkdir -p click/events
cp -r build/manifest.json click/manifest.json
cp security.json click/
cp -r data/images click/events
cp -r build/po/built_mos/locale click/events
cp build/src/scope.ini click/events/com.rubencarneiro.scopes.events_events.ini
cp build/src/libcom.rubencarneiro.scopes.events_events.so click/events/libcom.rubencarneiro.scopes.events_events.so

click build ./click
