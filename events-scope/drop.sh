#!/bin/bash

set -e
set -x

SRC=unity-scope-events
TEMP=/tmp/$SRC
DEST=/home/phablet/$SRC

./clean.sh
cd ..
adb shell rm -rf $DEST
adb shell mkdir $DEST

rm -rf $TEMP
cp -r $SRC $TEMP
rm -rf $TEMP/.bzr

adb push $TEMP $DEST
adb shell chown -R phablet:phablet $DEST
cd $SRC
adb shell
