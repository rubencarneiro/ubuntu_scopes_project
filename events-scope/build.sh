#!/bin/bash

if [ "$#" -eq 1 ]; then
    rm -rf build
fi

mkdir -p build
cd build
cmake .. && make
