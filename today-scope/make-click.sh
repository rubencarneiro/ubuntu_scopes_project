#!/bin/bash
set -e

# general operations
# NOTE: changing SOURCE dir requires changing po/POTFILES.in paths
SOURCE="click-src"
TARGET="click"
TIMESTAMP=$(date +%Y-%m-%dT%H:%M:%S)

# manifest
PKG="com.canonical.scopes.dashboard"
APP="dashboard"
TITLE="Today"
DESC="All your info in one place"
VERSION="4.16.2"
FRAMEWORK="ubuntu-sdk-16.04"

# defination for test
IS_GO_SCOPE="False"
TEST="tests/autopilot"
PREFIX="com.canonical.scopes"
PKGNAME="dashboard"
TEST_DEPARTMENTS="False"
TEST_ALL_DEPARTMENTS_HAVE_RESULTS="False"
TEST_LOCATIONDATANEEDED="False"
TEST_SEARCHHINT="False"
TEST_PAGEHEADER_LOGO="False"
TEST_PAGEHEADER_BACKGROUND="False"
TEST_PAGEHEADER_FOREGROUNDCOLOR="False"
TEST_PAGEHEADER_DIVIDERCOLOR="False"
TEST_PREVIEWBUTTONCOLOR="False"
SET_INTERNET_CONNECTIVITY="True"
TEST_SEARCH="True" #NOTE, if True, also set SEARCH_STRING
SEARCH_STRING="today" #New Year's Eve at least
# Scopes are tested running with SearchMetadata.location set to this lat & lang
LAT="52.5167" # London. DO NOT LEAVE BLANK
LONG="13.3833" # same
COUNTRY="de"
# get target languages from po files
POTS=""
for entry in po/*.po
do
  POTS="$POTS""$entry"";"
done
POFILES=$(echo "${POTS%?}")

TEST_MO_FILES="True"
TEST_INI_TRANSLATIONS="True" # Only set to false during development
TEST_TRANSLATED_DISPLAYNAME="True"
TEST_TRANSLATED_DESCRIPTION="False"
TEST_RESULTS="True"
TEST_PREVIEWBUTTONCOLOR="False"
GETTEXT_DOMAIN="aggregator"
# pkg name
CLICK_PKG="${PKG}_${VERSION}_armhf.click"

# scope ini
DISPLAY_NAME="Today"
DESCRIPTION=$DESC
AUTHOR="Canonical, Ltd."
SCOPE_ICON="images\/icon.png"
SEARCH_HINT=""
LOCATION_AWARENESS="true"
RESULTS_TTL_TYPE="Large"

#remove intermediate generated files from any previous run
find $SOURCE/ -name "scope.ini.in" -exec rm -f {} \;
find $SOURCE/ -name "settings.ini" -exec rm -f {} \;

# backup target dir and make new one
[ -d $TARGET ] && mv $TARGET "/tmp/bak.${TARGET}_${TIMESTAMP}"
mkdir $TARGET
mkdir $TARGET/$APP

# convert po files to mo files in target
cd po
pos=$(ls | grep po$)
for po in $pos
do
    lang=$(echo $po | cut -f1 -d\.)
    mkdir -p ../$TARGET/$APP/locale/$lang/LC_MESSAGES/
    msgfmt -o ../$TARGET/$APP/locale/$lang/LC_MESSAGES/aggregator.mo $po
done
cd ..

# apparmor
cp $SOURCE/apparmor.json $TARGET/${PKG}_${APP}.apparmor.json

# manifest
sed 's_APP_'${APP}'_g' manifest.json > $TARGET/manifest.json
sed -i 's_PKG_'${PKG}'_g' $TARGET/manifest.json
sed -i 's_TITLE_'"${TITLE}"'_g' $TARGET/manifest.json
sed -i 's_DESC_'"${DESC}"'_g' $TARGET/manifest.json
sed -i 's_VERSION_'"${VERSION}"'_g' $TARGET/manifest.json
sed -i 's_FRAMEWORK_'"${FRAMEWORK}"'_g' $TARGET/manifest.json

#test
sed 's/@PKG_PREFIX@/'"${PREFIX}"'/g' $TEST/test_generic.py.in > $TEST/test_generic.py
sed -i 's/@PKG@/'"${PKGNAME}"'/g' $TEST/test_generic.py
sed -i 's/@APP@/'"${APP}"'/g' $TEST/test_generic.py
sed -i 's/@VERSION@/'"${VERSION}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_DEPARTMENTS@/'"${TEST_DEPARTMENTS}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_ALL_DEPARTMENTS_HAVE_RESULTS@/'"${TEST_ALL_DEPARTMENTS_HAVE_RESULTS}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_LOCATIONDATANEEDED@/'"${TEST_LOCATIONDATANEEDED}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_SEARCHHINT@/'"${TEST_SEARCHHINT}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_PAGEHEADER_LOGO@/'"${TEST_PAGEHEADER_LOGO}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_PAGEHEADER_BACKGROUND@/'"${TEST_PAGEHEADER_BACKGROUND}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_PAGEHEADER_FOREGROUNDCOLOR@/'"${TEST_PAGEHEADER_FOREGROUNDCOLOR}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_PAGEHEADER_DIVIDERCOLOR@/'"${TEST_PAGEHEADER_DIVIDERCOLOR}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_SEARCH@/'"${TEST_SEARCH}"'/g' $TEST/test_generic.py
sed -i 's/@SEARCH_STRING@/'"${SEARCH_STRING}"'/g' $TEST/test_generic.py
sed -i 's/@LAT@/'"${LAT}"'/g' $TEST/test_generic.py
sed -i 's/@LONG@/'"${LONG}"'/g' $TEST/test_generic.py
sed -i 's#@POFILES@#'"${POFILES}"'#g' $TEST/test_generic.py
sed -i 's/@TEST_MO_FILES@/'"${TEST_MO_FILES}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_INI_TRANSLATIONS@/'"${TEST_INI_TRANSLATIONS}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_TRANSLATED_DISPLAYNAME@/'"${TEST_TRANSLATED_DISPLAYNAME}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_TRANSLATED_DESCRIPTION@/'"${TEST_TRANSLATED_DESCRIPTION}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_RESULTS@/'"${TEST_RESULTS}"'/g' $TEST/test_generic.py
sed -i 's/@SET_INTERNET_CONNECTIVITY@/'"${SET_INTERNET_CONNECTIVITY}"'/g' $TEST/test_generic.py
sed -i 's/@IS_GO_SCOPE@/'"${IS_GO_SCOPE}"'/g' $TEST/test_generic.py
sed -i 's/@TEST_PREVIEWBUTTONCOLOR@/'"${TEST_PREVIEWBUTTONCOLOR}"'/g' $TEST/test_generic.py
sed -i 's/@GETTEXT_DOMAIN@/'"${GETTEXT_DOMAIN}"'/g' $TEST/test_generic.py

# scope.ini
# replace values
sed 's/DISPLAY_NAME/'"${DISPLAY_NAME}"'/g' $SOURCE/aggregator/scope.ini.in.in > $SOURCE/aggregator/scope.ini.in
sed -i 's/DESCRIPTION/'"${DESCRIPTION}"'/g' $SOURCE/aggregator/scope.ini.in
sed -i 's/AUTHOR/'"${AUTHOR}"'/g' $SOURCE/aggregator/scope.ini.in
sed -i 's/SCOPE_ICON/'"${SCOPE_ICON}"'/g' $SOURCE/aggregator/scope.ini.in
sed -i 's/SEARCH_HINT/'"${SEACH_HINT}"'/g' $SOURCE/aggregator/scope.ini.in
sed -i 's/LOCATION_AWARENESS/'"${LOCATION_AWARENESS}"'/g' $SOURCE/aggregator/scope.ini.in
sed -i 's/RESULTS_TTL_TYPE/'"${RESULTS_TTL_TYPE}"'/g' $SOURCE/aggregator/scope.ini.in
# localize
intltool-merge -d -u -q ./po/ ${SOURCE}/aggregator/scope.ini.in $SOURCE/aggregator/scope.ini

cp $SOURCE/aggregator/scope.ini $TARGET/$APP/${PKG}_${APP}.ini

# settings.ini
#  NOT USING SETTINGS
#intltool-merge -d -u -q ./po/ ${SOURCE}/aggregator/settings.ini.in $SOURCE/aggregator/settings.ini
#cp $SOURCE/aggregator/settings.ini $TARGET/$APP/${PKG}_${APP}-settings.ini

# binary
cp $SOURCE/aggregator/libcom.canonical.scopes.aggregator_aggregator.so $TARGET/$APP/lib${PKG}_${APP}.so

#child_scoess.json
cp $SOURCE/aggregator/child_scopes.json $TARGET/$APP/child_scopes.json

#hints.json
cp $SOURCE/aggregator/hints.json $TARGET/$APP/hints.json

#images
cp -r $SOURCE/aggregator/images $TARGET/$APP/

click build $TARGET

if [ "$1" = "install" ]; then
    adb push $CLICK_PKG /tmp && adb shell pkcon install-local --allow-untrusted /tmp/$CLICK_PKG
    adb shell rm -rf /home/phablet/autopilot/$APP
    adb shell mkdir -p /home/phablet/autopilot/$APP
    adb push $TEST/scope-data /home/phablet/autopilot/
    adb push $TEST/__init__.py /home/phablet/autopilot/$APP
    adb push $TEST/test_generic.py /home/phablet/autopilot/$APP
fi
