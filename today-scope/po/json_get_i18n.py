#!/usr/bin/python3
import json
import os
import sys

idx = -1
trans = dict();

def dolist(alist ):
    for l in alist:
        if type(l) is list:
            dolist(l)
        if type(l) is dict:
            dodict(l)

def dodict(adict):
    for k,v in adict.items():
        if k.startswith("_"):
            global idx
            idx += 1
            global tran
            trans[k + str(idx)] = v
        if type(v) is list:
            dolist(v)
        if type(v) is dict:
            dodict(v)

if len(sys.argv) < 2:
    print("File arg missing. Stopping")
    sys.exit(1)

if os.path.exists("i18n.json"):
    os.remove("i18n.json")

print("the filel:", sys.argv[1])

with open(sys.argv[1]) as j_file:
    j = json.load(j_file)

if type(j) is list:
    dolist(j)
if type(j) is dict:
    dodict(j)

with open("i18n.json", "w") as outfile:
    json.dump(trans, outfile, indent=4)

#print(json.dumps(trans, indent=4))
