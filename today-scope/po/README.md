Hi,

You can update the pot file and then the po files.

And you can add strings for translation

# Adding strings for translation

There are three places where translatable string can be added:

* The scope ini and the settings ini.
* department and category titles you've declared in child_scopes.json.

## Scope ini file

Please add strings of interest into the make-click.sh script appropriate variables only.

So nothing in scope ini src needs to be edited/modified, just make-click.sh..

## settings ini file

Only one translatable string is currenlty supported and is for user settings to control the number of results for each scope. 

So nothing in settings ini src needs to be edited/modified.

## child_scopes.json

Here you may declared various strings displayed to the user. These need to be included for translation. For example you may declared department and category titles.

Due to a current limitation in the tool chain (json2po), child_scopes.json cannot be scanned directly. Therefore, please ensure all such translable strings are present as *values* in translatables.json. Every key must start with an underscore or it is ignored. Other than starting with an underscore, the key name has no signifance.  And keep it simple: only text values (to avoid the json2po limitation.

You can find all such strings in child-scopes.json easily because by design their keys all start with an underscore, so you can run a command like this:

        grep "\"_" click-src/aggregator/child_scopes.json
                "_title": "Headlines"
                "_title": "International News"
                "_title": "National News"
                "_title": "Technology"
                "_title": "Sports"
                "_title": "Finance"

# Updating the pot file

1. Be in the po/ directory
1. Run ./make-pot.sh
1. Review the generated PROJECT.pot file to ensure it inlcudes the expected strings

# Updating the po files

Warning: Updating po files should only be done when strings are final. Doing so may delete translations for strings that no longer exist. If you bring any such string back, its translation may have been lost, which wastes translator time.

WARNING: if the project is translated on launchpad, if you update the po filesi, you need to be sure the LP project pulls them in or they will not be presented to the community for translation work.

1. Be in the po/ directory
1. Run ./update-pos-from-pot.sh
1. Review the a po file to ensure it inlcudes the expected strings

Cheers,
Kyle Nitzsche
June 2015

