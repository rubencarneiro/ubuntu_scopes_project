# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
# #-#-#-#-#  de.po (unity-scope-dashboard)  #-#-#-#-#
# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
# #-#-#-#-#  de.po (unity-scope-dashboard)  #-#-#-#-#
# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
# #-#-#-#-#  de.po (unity-scope-dashboard)  #-#-#-#-#
# #-#-#-#-#  de.po (photos)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
# #-#-#-#-#  de.po (unity-scope-dashboard)  #-#-#-#-#
# German translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: unity-scope-dashboard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-10-17 10:14-0400\n"
"PO-Revision-Date: 2016-07-11 17:13+0000\n"
"Last-Translator: schuko24 <gerdsaenger@t-online.de>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-10-18 07:05+0000\n"
"X-Generator: Launchpad (build 18232)\n"
"Language: de\n"

#: ._title4
msgid "See your events and more from Google."
msgstr "Zeigen Sie Ihre Termine und mehr von Google an."

#: ._title5
msgid "<b>   Review your Google sync settings</b>"
msgstr "<b>   Überprüfen Sie Ihre Google-Synchronisierungseinstellungen</b>"

#: ._title0
msgid "Welcome to the Today scope"
msgstr "Willkommen im Bereich \"Heute\""

#: ._title1
msgid "Manage your day with all the essentials."
msgstr "Verwalten Sie Ihren Tag mit allem was wichtig ist."

#: ._title13
msgid "<b>Skip &amp; set up later</b>"
msgstr "<b>Überspringen &amp; und später einrichten</b>"

#: ._loggedin12
msgid "Get started."
msgstr "Erste Schritte."

#: ._name6 ._name10
msgid "Add account"
msgstr "Konto hinzufügen"

#: ._title9
msgid "<b>   Add your account</b>"
msgstr "<b>   Ihr Konto hinzufügen</b>"

#: ._subtitle3
msgid "Tap here for more information."
msgstr "Tippen Sie hier, um mehr Informationen zu erhalten."

#: ._title8
msgid "Track your activity with Fitbit."
msgstr "Verfolgen Sie Ihre Aktivität mit Fitbit."

#: ._title11
msgid "Set up your accounts later."
msgstr "Richten Sie Ihre Konten später ein."

#: ._description7
msgid ""
"Adding your Google account will allow Today to display your events and "
"contacts.<br><br>Press the button below to add your account (or go to "
"accounts in the Settings app).<br><br>Remember to enable the contact and "
"calendar sync once you have successfully added it."
msgstr ""
"Wenn Sie Ihr Google-Konto hinzufügen, können Sie Ihre Termine und Kontakte "
"unter \"Heute\" anzeigen.<br><br>Drücken Sie die Schaltfläche unten, um Ihr "
"Konto hinzuzufügen (oder gehen Sie in der Einstellungen-App zu "
"\"Konten\").<br><br>Denken Sie daran, die Kontakt- und "
"Kalendersynchronisierung zu aktivieren, sobald Sie sie erfolgreich "
"hinzugefügt haben."

#: ._description2
msgid ""
"Today gives you a overview of your day including:<br>- events and tasks<br>- "
"calls and messages<br>- news, weather and more.<br><br> You can customise "
"what is shown in Today using the scope settings icon.<br><br> Some services "
"require your log-in below or later."
msgstr ""
"\"Heute\" bietet Ihnen einen Überblick über Ihren Tag, einschließlich:<br>- "
"Termine und Aufgaben<br>- Anrufe und Mitteilungen<br>- Nachrichten, Wetter "
"und mehr<br><br> Sie können mittels des Bereichseinstellungen-Symbols "
"anpassen, was unter \"Heute\" angezeigt wird.<br><br> Für einige Services "
"müssen Sie sich unten oder später anmelden."

#: ._48
msgid "Holidays"
msgstr ""

#: ._75
msgid "Weather"
msgstr "Wetter"

#: ._52
msgid "Favorite Contacts"
msgstr "Bevorzugte Kontakte"

#: ._13
msgid "Recent"
msgstr "Kürzlich"

#: ._64
msgid "My Activity"
msgstr "Meine Aktivitäten"

#: ._97
msgid "Trending on Twitter"
msgstr "Trends auf Twitter"

#: ._81
msgid "Today's News"
msgstr "Nachrichten von heute"

#: ._20 ../click-src/aggregator/scope.ini.in.h:1
msgid "Today"
msgstr "Heute"

#: ._36
msgid "All your info in one place"
msgstr "Alle Ihre Informationen an einem Ort"

#: ../click-src/aggregator/scope.ini.in.h:2
msgid "IsAggregator=true"
msgstr ""
