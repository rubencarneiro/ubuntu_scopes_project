# Galician translations for PACKAGE package.
# Copyright (C) 2014 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: texts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-10-25 14:13-0400\n"
"PO-Revision-Date: 2016-10-25 23:28+0000\n"
"Last-Translator: Marcos Lans <Unknown>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-10-26 05:54+0000\n"
"X-Generator: Launchpad (build 18246)\n"
"Language: gl\n"

#: ../data/scope.ini.in.h:1
msgid "Messages"
msgstr "Mensaxes"

#: ../data/scope.ini.in.h:2
msgid "Message History"
msgstr "Historial das mensaxes"

#: ../data/scope.ini.in.h:3
msgid "Name or Date"
msgstr "Nome ou data"

#. create categories for each surfacing time frame
#: ../src/query.cpp:201 ../src/query.cpp:339 ../src/query.cpp:340
msgid "Today"
msgstr "Hoxe"

#. Translators: unknown sender, meaning there is no information about the person who sent the message
#: ../src/query.cpp:231
msgid "Unknown"
msgstr "Descoñecido"

#. Translators: private sender, meaning the person who sent the message has kept their phone number info private
#: ../src/query.cpp:235
msgid "Private"
msgstr "Privado"

#: ../src/query.cpp:269
msgid "Time"
msgstr "Hora"

#: ../src/query.cpp:271
msgid "Date"
msgstr "Data"

#: ../src/query.cpp:273
msgid "Placed"
msgstr "Realizadas"

#: ../src/query.cpp:281 ../src/query.cpp:310
msgid "Received"
msgstr "Recibidas"

#. Translators: This is the title of the text type filter. This filter lets th    e user display only the specified type of text: received, or sent
#: ../src/query.cpp:309
msgid "Text type"
msgstr "Tipo de mensaxes"

#: ../src/query.cpp:311
msgid "Sent"
msgstr "Enviadas"

#: ../src/query.cpp:341
msgid "Previous 7 days"
msgstr "7 días anteriores"

#: ../src/query.cpp:342
msgid "Previous 30 days"
msgstr "30 días anteriores"

#: ../src/query.cpp:445
msgid "No Received Messages Today"
msgstr "Hoxe non se recibiron mensaxes"

#: ../src/query.cpp:458
msgid "No Messages Today"
msgstr "Hoxe non houbo mensaxes"

#. Translators: "Call" is a verb, as in to dial a phone number
#: ../src/query.cpp:511
msgid "Call"
msgstr "Chamar"

#. Translators: "Message" is a verb, as in to send a text message
#: ../src/query.cpp:513
msgid "Message"
msgstr "Enviar mensaxe"

#: ../src/query.cpp:528
msgid "Number"
msgstr "Número"
