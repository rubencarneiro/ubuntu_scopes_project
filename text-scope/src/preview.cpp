/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * Authored by: Kyle Nitzsche <kyle.nitzsche@canonical.com>
 *
 */

#include"preview.h"
#include "i18n.h"

#include <unity/scopes/PreviewWidget.h>
#include <unity/scopes/ColumnLayout.h>
#include <unity/scopes/PreviewReply.h>
#include <unity/scopes/VariantBuilder.h>
#include <unity/scopes/Variant.h>
#include <unity/UnityExceptions.h>

#include <QDebug>
#include <map>
#include <string>
#include <iostream>
#include <fstream>

using namespace unity::scopes;

Preview::Preview(Result const& result, ActionMetadata const& metadata) :
    PreviewQueryBase(result, metadata),
    result_(result)
{
}

Preview::~Preview()
{
}

void Preview::cancelled()
{
}

void Preview::run(unity::scopes::PreviewReplyProxy const& reply)
{
    ColumnLayout layout1col(1);
    std::vector<std::string> ids = {"headerId", "callType", "messageId", "phoneId", "artId", "actionsId", "detailsId"};

    ColumnLayout layout2col(2);
    layout2col.add_column({"headerId", "callType", "messageId", "phoneId", "artId"});
    layout2col.add_column({"actionsId", "detailsId"});

    ColumnLayout layout3col(3);
    layout3col.add_column({"headerId", "callType", "messageId", "phoneId", "artId"});
    layout3col.add_column({"actionsId", "detailsId"});
    layout3col.add_column({});

    PreviewWidgetList widgets;

    PreviewWidget w_header("headerId", "header");
    w_header.add_attribute_mapping("title", "title");
    widgets.emplace_back(w_header);

    PreviewWidget w_message("messageId", "text");
    w_message.add_attribute_mapping("text", "message");
    widgets.emplace_back(w_message);

    PreviewWidget w_phone("phoneId", "text");
    w_phone.add_attribute_mapping("text", "phone");
    widgets.emplace_back(w_phone);

    PreviewWidget w_image("artId", "image");
    w_image.add_attribute_mapping("source", "art_");
    w_image.add_attribute_value("zoomable", Variant(true));
    widgets.emplace_back(w_image);

    PreviewWidget w_details("detailsId", "text");
    w_details.add_attribute_mapping("text", "details");
    widgets.emplace_back(w_details);

    PreviewWidget w_callType("callType", "text");
    w_callType.add_attribute_mapping("text", "type");
    widgets.emplace_back(w_callType);

    PreviewWidget w_actions("actionsId", "actions");
    w_actions.add_attribute_mapping("actions", "actions");
    widgets.emplace_back(w_actions);

    layout1col.add_column(ids);
    reply->register_layout({layout1col, layout2col, layout3col});
    reply->push(widgets);
}
