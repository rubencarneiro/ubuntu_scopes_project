/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * Authored by: Kyle Nitzsche <kyle.nitzsche@canonical.com>
 *
 */

#include "query.h"
#include "utils.h"
#include "i18n.h"

#include <unity/scopes/CategorisedResult.h>
#include <unity/scopes/CategoryRenderer.h>
#include <unity/scopes/QueryBase.h>
#include <unity/scopes/SearchReply.h>
#include <unity/scopes/CannedQuery.h>
#include <unity/scopes/VariantBuilder.h>
#include <unity/scopes/Variant.h>
#include <unity/scopes/OptionSelectorFilter.h>
#include <unity/scopes/FilterOption.h>

#include <QDebug>
#include <QStringList>
#include <QLocale>
#include <Qt>
#include <QCoreApplication>

#include <History/manager.h>
#include <History/eventview.h>
#include <History/types.h>
#include <History/sort.h>
#include <History/filter.h>
#include <History/textevent.h>

#include <locale.h>

namespace us = unity::scopes;
using namespace History;

static const std::string RENDERER = R"(
{
  "schema-version": 1,
  "template": {
    "category-layout": "vertical-journal",
    "card-layout": "horizontal",
    "card-size": "large"
  },
  "components": {
    "title": "title",
    "summary": "message",
    "attributes": {"field": "attributes", "max-count": 4},
    "art":  "art_",
    "mascot":  "mascot"
  }
}
)";

static const std::string RENDERER_NO_RESULTS = R"(
{
  "schema-version": 1,
  "template": {
    "category-layout": "grid",
    "card-layout": "horizontal",
    "card-size": "small"
  },
  "components": {
    "title": "title",
    "mascot": "mascot"
  }
}
)";

QString MESSAGE_ICON;
QString MESSAGE_IN_ICON;
QString MESSAGE_OUT_ICON;
QString UNKNOWNICON;

Query::Query(const us::CannedQuery &query, const us::SearchMetadata & metadata, Scope &scope ) :
    SearchQueryBase(query, metadata),
    cannedQuery(query),
    scope_(scope),
    query_(query.query_string())
{

    MESSAGE_ICON = QString("%1/images/message.svg").arg(QString::fromStdString(scope_.scope_directory()));
    MESSAGE_IN_ICON = QString("%1/images/messageIn.svg").arg(QString::fromStdString(scope_.scope_directory()));
    MESSAGE_OUT_ICON = QString("%1/images/messageOut.svg").arg(QString::fromStdString(scope_.scope_directory()));
    UNKNOWNICON = QString("%1/images/unknownContact.svg").arg(QString::fromStdString(scope_.scope_directory()));

    textType_m[TextType_t::placed] = "placed"; 
    textType_m[TextType_t::received] = "received"; 
}

Query::~Query()
{
}

void Query::cancelled()
{
}

QString strong(QString str) {
    return QString("<strong>%1</strong>").arg(str);
}

Events Query::getEventsForDateRange(QDate start, QDate end, EventViewPtr eView, Events &ev_l)
{

    qDebug() << "=== TEXTS in getEventsforDateRange";
    qDebug() << "=== TEXTS  start range: " << start;
    qDebug() << "=== TEXTS end range: " << end;
    Events l_toadd;
    try {
        bool outOfBounds = false;
        // start by parsing the existing events
        Q_FOREACH(const Event &event, ev_l) {
            QDate ev_date = event.timestamp().date();

            // if the date is newer than the end date, keep searching
            // as the results are sorted from newer to older
            if (ev_date > end) {
                continue;
            }

            // now if the date is older than the end date, we are already
            // out of bounds and can stop processing
            if (ev_date < start) {
                break;
            }

            // if we got here, we want the event
            l_toadd.append(event);
            qDebug() << "=== TEXTS. extant events. adding event timestamped: " << ev_date.toString();
        }

        bool going = true;
        while (going) {

            Events l = eView->nextPage();

            if (l.size() == 0)
                going = false;

            // append the events to the existing ones for the next query
            ev_l.append(l);

            // and process them too
            Q_FOREACH(const Event &event, l) {
                QDate ev_date = event.timestamp().date();

                // if the date is newer than the end date, keep searching
                // as the results are sorted from newer to older
                if (ev_date > end) {
                    continue;
                }

                // now if the date is older than the end date, we are already
                // out of bounds and can stop processing
                if (ev_date < start) {
                    going = false;
                    break;
                }
                // if we got here, we want the event
                l_toadd.append(event);
                qDebug() << "=== TEXTS. new events. adding event timestamped: " << ev_date.toString();
            }
        }
    }
    catch (std::exception &e){
        qDebug() << "=== TEXTS exception caught";
    }
    qDebug() << "=== TEXTS: num events: " << QString::number(l_toadd.size());
    qDebug() << "=== TEXTS ENDING getEventsforDateRange";
    return l_toadd;
}

void Query::processEvents(Events &ev_l, std::set<std::string> &text_types)
{
    qDebug()<<"==== TEXTS. process. num events: " << QString::number(ev_l.size());
    //loop through voice events and build data structures
    for (TextEvent ev : ev_l) {
        tevent_t tevent;
        QDate cDate = ev.timestamp().date();
        QString date_s = cDate.toString("dd-MM-yyyy");
        QTime time = ev.timestamp().time();
        QString time_s = time.toString("hh:mm");


        //decide which category to use based on the call date
        if (cDate == today){
            date_s = _("Today");
            cat = cat1;//the categoryRenderer for today
            tevents = &tevents1;//the list of *surfaced* calls for today, not including dups ("collapsed" calls)
            ids = &ids1;//the set of ids for surfaced calls.
            id_collEvs = &id_collEvs1;//maps the surfaced call id to a list of its collapsed calls
        } else if (cDate >= sevenDaysStart) {
            cat = cat7;
            tevents = &tevents7;
            ids = &ids7;
            id_collEvs = &id_collEvs7;
        } else if (cDate >= thirtyDaysStart) {
            cat = cat30;
            tevents = &tevents30;
            ids = &ids30;
            id_collEvs = &id_collEvs30;
        }

        tevent.cat = cat;

        //get the first participant from the conversation
        //FIXME: add support for group conversations
        Participant participant = ev.participants().first();
        QString callerId = participant.identifier();
        QString alias = participant.alias().isEmpty() ? callerId : participant.alias();
        QString sender = ev.senderId();

        QString query = qstr(query_);
        if (participant.contactId().isEmpty()) { //not a contact
            if (callerId == "x-ofono-unknown") {
                //Translators: unknown sender, meaning there is no information about the person who sent the message
                alias = qstr(_("Unknown"));
            }
            else if (callerId  == "x-ofono-private") {
                //Translators: private sender, meaning the person who sent the message has kept their phone number info private
                alias = qstr(_("Private"));
            }
            tevent.art = UNKNOWNICON;
            tevent.phone = alias;
            if (searchingName) { //if searching on name, continue if not found, allow searcing on _("Unknown")
                if (!alias.contains(query,Qt::CaseInsensitive))
                    continue;
            }
        } else { //is a contact
            if (searchingName) {//search by first or last name in any order or fragments thereof allowed
                bool match = true;
                Q_FOREACH(const QString &part, parts) {
                    if (!alias.contains(part, Qt::CaseInsensitive)) {
                        match = false;
                        break;
                    }
                }
                if (!match) {
                    continue;
                }
            }
            QString avatar = participant.avatar();
            std::string callerIconUrl;
            if (avatar.isEmpty()) {
                avatar = UNKNOWNICON;
            }
            tevent.art = avatar;
        }

        // continue if searching on date and it is not matched
        if ((searchingDate) && (cDate != searchDate))
            continue;

        tevent.caller = alias;
        tevent.time = QString("%1: %2").arg(strong(qstr(_("Time")))).arg(time_s);
        tevent.timeShort = time_s;
        tevent.date = QString("%1: %2").arg(strong(qstr(_("Date")))).arg(date_s);
        tevent.dateShort = date_s;
        tevent.typeMsg = strong(_("Placed"));

        if (ev.senderId() == "self") {
            tevent.type = TextType_t::placed;
            tevent.mascot = MESSAGE_OUT_ICON;
        } else {
            tevent.type = TextType_t::received;
            tevent.mascot = MESSAGE_IN_ICON;
            tevent.typeMsg = strong(_("Received"));
        }

        // do not show texts if they do not match user text type filter, if any
        if (!text_types.empty()) {
              if (text_types.find(textType_m[tevent.type]) == text_types.end())  
			continue;
        }

        tevent.msgUri = QString("message:///%1").arg(callerId);
        tevent.callUri = QString("tel:///%1").arg(callerId);

        tevent.message = ev.message();

        QString id = QString("%2-%3").arg(callerId).arg(tevent.typeMsg);
        count++;
        tevent_t * ce;
        tevent.collapseId = id;
        ids->append(id);
        tevents->append(tevent);
    }
}

void Query::run(us::SearchReplyProxy const& reply)
{

    auto filter_state = query().filter_state();
    //Translators: This is the title of the text type filter. This filter lets th    e user display only the specified type of text: received, or sent
    us::OptionSelectorFilter::SPtr textType_filter = us::OptionSelectorFilter::create("texttype", _("Text type"), true);
    textType_filter->add_option("received", _("Received"));
    textType_filter->add_option("placed", _("Sent"));

    reply->push({textType_filter}, query().filter_state());

    if (textType_filter->has_active_option(filter_state)) {
        auto textTypes = textType_filter->active_options(filter_state);
        for (auto const &opt: textTypes) {
            text_types.insert(opt->id());
        }
    }

    textdomain(GETTEXT_DOMAIN.toStdString().c_str());
    const QString scopePath = QString::fromStdString(scope_.scope_directory());
    QString tdir = QString("%1/locale/").arg(scopePath);
    QString basedir = QString(bindtextdomain(GETTEXT_DOMAIN.toStdString().c_str(), tdir.toStdString().c_str()));

    setlocale(LC_ALL, "");

    // QtContacts uses d-bus so we have to set this up even though we don't
    // use it, per pattern in unity-scope-contacts
    int argc = 1;
    char* argv = "texts-scope";
    QCoreApplication *app = new QCoreApplication(argc, &argv);

    us::CategoryRenderer renderer(RENDERER);
    us::CategoryRenderer renderer_no_results(RENDERER_NO_RESULTS);

    //create categories for each surfacing time frame
    cat1_no_results = reply->register_category("today_no_results", _("Today"), "", renderer_no_results);
    cat1 = reply->register_category("today", _("Today"), "", renderer);
    cat7 = reply->register_category("previous7", _("Previous 7 days"), "", renderer);
    cat30 = reply->register_category("previous30", _("Previous 30 days"), "", renderer);
    //create pointer to category. This will point at the right/current category based
    //on the date of each call
    cat;

    parts;
    searchDate;
    searchingDate = false;
    searchingName = false;
    // handle search:
    if (query_ != ""){
        QString qry = qstr(query_);
        //let query use spaces or dashes to delimit token
        QStringList partsSpaces = qry.split(" ");
        QStringList partsDashes = qry.split("-");
        if (partsSpaces.size() >= partsDashes.size())
            parts = partsSpaces;
        else
            parts = partsDashes;
        //when user enters delimeter char (and before they enter more)
        //delete the final empty string
        if ((parts.size() > 1) && (parts[parts.size()-1] == ""))
            parts.removeLast();
        int dy = 0;
        int mo = 0;
        int yr = 0;
        if (parts.size() > 0) {
            bool searchStartsWithInt;
            bool *anInt = & searchStartsWithInt;
            dy = parts[0].toInt(anInt);
            if (searchStartsWithInt) {
                //treat search as date
                searchingDate = true;
                if (parts.size() > 1)
                    mo = parts[1].toInt();
                if (parts.size() > 2)
                    yr = parts[2].toInt();
                QDate date = QDate::currentDate();
                if (mo == 0 )
                    mo = date.month();
                if (yr < 2000)
                    yr = date.year();
                searchDate = QDate(yr, mo, dy);
            } else {
                //treat search as name
                searchingName = true;
            }
        }
    }

    today = QDate::currentDate();
    sevenDaysEnd = today.addDays(-1);
    sevenDaysStart = today.addDays(-7);
    thirtyDaysEnd = sevenDaysStart.addDays(-1);
    thirtyDaysStart = today.addDays(-37);

    const Sort sort = Sort("timestamp", Qt::SortOrder::DescendingOrder,Qt::CaseInsensitive);
    Manager *manager = Manager::instance();
    EventViewPtr eView = manager->queryEvents(EventType::EventTypeText, sort);
    Events ev_l;

    Events filtered = getEventsForDateRange(today, today, eView, ev_l);
    processEvents(filtered, text_types);

    bool agged = false;
    if (search_metadata().is_aggregated())
    {
        auto keywords = search_metadata().aggregated_keywords();
        if (keywords.find("recent") != keywords.end())
        {
            agged = true;
            qDebug() << "==== TEXTS is agg'd by keyword: recent";
            for (auto ev : tevents1)
            {
                if (ev.type == TextType_t::received)
                {
                    us::CategoryRenderer renderer(RENDERER);
                    auto cat = reply->register_category("aggregated_keyword+one_message", "Today", "", renderer_no_results);
                    unity::scopes::CategorisedResult res(cat);
                    res.set_title(ev.caller.toStdString());
                    res.set_uri("scope://com.canonical.scopes.texts_texts");
                    res["mascot"] = MESSAGE_ICON.toStdString();
                    us::VariantBuilder attrs;
                    attrs.add_tuple({
                        {"value", us::Variant(QString("%1").arg(ev.message).toStdString())}
                    });
                    attrs.add_tuple({
                        {"value", us::Variant("")}
                    });
                    attrs.add_tuple({
                        {"value", us::Variant(QString("%1 %2").arg(ev.dateShort, ev.timeShort).toStdString())}
                    });
                    attrs.add_tuple({
                        {"value", us::Variant("")}
                    });
                    res["attributes"] = attrs.end();

                    reply->push(res);
                    return;
                }
            }
            qDebug() << "=== TEXTS NONE received today";
            us::CategorisedResult res(cat1_no_results);
            res["title"] = _("No Received Messages Today");
            res.set_uri("scope://com.canonical.scopes.texts_texts");
            res["mascot"] = MESSAGE_ICON.toStdString();
            reply->push(res);
            return;
        }
    }
    if (!agged)
    {
        qDebug() << "== texts not agged";
        // if no messages today, send generated result saying No Messages Today
        if (tevents1.size() == 0){
            us::CategorisedResult res_(cat1_no_results);
            res_["title"] = _("No Messages Today");
            res_.set_uri("null");
            reply->push(res_);
        } else
            Query::pushReplies(tevents1, id_collEvs1, reply);

        filtered = getEventsForDateRange(sevenDaysStart, sevenDaysEnd, eView, ev_l);
        processEvents(filtered, text_types);
        Query::pushReplies(tevents7, id_collEvs7, reply);

        filtered = getEventsForDateRange(thirtyDaysStart, thirtyDaysEnd, eView, ev_l);
        processEvents(filtered, text_types);
        Query::pushReplies(tevents30, id_collEvs30, reply);
    }
}

void Query::pushReplies(QList<tevent_t> &tevents, QMap<QString,QList<collapsedEvent_t>> &id_collEvs, us::SearchReplyProxy const& reply) {
    QListIterator<Query::tevent_t> iter(tevents);


    while (iter.hasNext()){
        tevent_t tev = iter.next();
        us::CategorisedResult res(tev.cat);


        //details holds call details for at least one call, but then also for
        //collapsed calls, if any
        QString details = QString("%1 %2").arg(tev.time).arg(tev.date);

        res.set_title(tev.caller.toStdString());

        res["details"] = details.toStdString();
        res["message"] = tev.message.toStdString();

        us::VariantBuilder attrs;
        attrs.add_tuple({
            {"value", us::Variant(QString("%1 %2").arg(tev.timeShort, tev.dateShort).toStdString())}
        });
        attrs.add_tuple({
            {"value", us::Variant("")}
        });
        res["attributes"] = attrs.end();

        res["art_"] = tev.art.toStdString();
        // 'art_' used (instead of art) to enable disambiguation in aggregator
        // scope. Do not change field name.
        res["mascot"] = tev.mascot.toStdString();
        res["type"] = tev.typeMsg.toStdString();


        us::VariantBuilder actions;

        //Translators: "Call" is a verb, as in to dial a phone number
        std::string call = _("Call");
        //Translators: "Message" is a verb, as in to send a text message
        std::string message = _("Message");

        actions.add_tuple({
                              {"id", us::Variant("call")},
                              {"label", us::Variant(call)},
                              {"uri", us::Variant(tev.callUri.toStdString())}
                          });
        actions.add_tuple({
                              {"id", us::Variant("sms")},
                              {"label", us::Variant(message)},
                              {"uri", us::Variant(tev.msgUri.toStdString())}
                          });
        res["actions"]=actions.end();

        res.set_uri(tev.callUri.toStdString());
        res["phone"] = QString("%1: %2").arg(strong(_("Number"))).arg(tev.phone).toStdString();
        res["message"] = QString("%1").arg(tev.message).toStdString();
        reply->push(res);
    }
}
