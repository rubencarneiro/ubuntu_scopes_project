/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License version 3 as * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the * GNU General Public License for more details.
 * * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * Authored by: Kyle Nitzsche <kyle.nitzsche@canonical.com>
 * */

#ifndef QUERY_H
#define QUERY_H

#include "scope.h"
#include <unity/scopes/SearchQueryBase.h>
#include <unity/scopes/ReplyProxyFwd.h>
#include <QString>
#include <QList>
#include <QStringList>
#include <QMap>
#include <Qt>

#include <History/manager.h>
#include <History/eventview.h>
#include <History/types.h>
#include <History/sort.h>
#include <History/filter.h>
#include <History/voiceevent.h>

class Query : public unity::scopes::SearchQueryBase
{
public:
    Query(const unity::scopes::CannedQuery &query, const unity::scopes::SearchMetadata &, Scope &);
    ~Query();
    virtual void cancelled() override;

    virtual void run(unity::scopes::SearchReplyProxy const& reply) override;

private:

    bool searchingDate;
    bool searchingName;
    QStringList parts;
    QDate searchDate;

    QDate today;
    QDate sevenDaysStart;
    QDate sevenDaysEnd;
    QDate thirtyDaysStart;
    QDate thirtyDaysEnd;

    unity::scopes::Category::SCPtr cat;
    unity::scopes::Category::SCPtr cat1_no_results;
    unity::scopes::Category::SCPtr cat1;
    unity::scopes::Category::SCPtr cat7;
    unity::scopes::Category::SCPtr cat30;

    History::Events getEventsForDateRange(QDate start, QDate end, History::EventViewPtr eView, History::Events &ev_l);

    std::set<std::string> text_types;
    void processEvents(History::Events &ev_l, std::set<std::string> &text_types);

    QList<unity::scopes::VariantArray> collapsedArrays_varr;
    QList<unity::scopes::VariantMap> collapsedMaps_varr;
    Scope & scope_;
    const unity::scopes::CannedQuery cannedQuery;
    std::string query_;
    enum class TextType_t {placed, received};
    std::map<TextType_t, std::string> textType_m;

    struct collapsedEvent_t {
        QString time;
        QString date;
        QString duration;
    };

    int count = 0;
    struct tevent_t {
        QString time;
        QString timeShort;
        QString date;
        QString dateShort;
        unity::scopes::Category::SCPtr cat;
        QString mascot;
        QString art;
        QString caller;
        TextType_t type;//missed, placed
        QString typeMsg;
        QString callUri;
        QString msgUri;
        QString phone;
        QString collapseId;
        QString message;
    };

    QStringList *ids;
    QStringList ids1;
    QStringList ids7;
    QStringList ids30;
    QStringList idsOlder;

    QMap<QString,QList<collapsedEvent_t>> *id_collEvs;
    QMap<QString,QList<collapsedEvent_t>> id_collEvs1;
    QMap<QString,QList<collapsedEvent_t>> id_collEvs7;
    QMap<QString,QList<collapsedEvent_t>> id_collEvs30;
    QMap<QString,QList<collapsedEvent_t>> id_collEvsOlder;

    QList<tevent_t> *tevents;
    QList<tevent_t> tevents1;
    QList<tevent_t> tevents7;
    QList<tevent_t> tevents30;
    QList<tevent_t> teventsOlder;

    void pushReplies (QList<Query::tevent_t> &events, QMap<QString,QList<collapsedEvent_t>> &id_collEvs, unity::scopes::SearchReplyProxy const& reply);

};
#endif
